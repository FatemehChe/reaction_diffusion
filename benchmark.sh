#!/bin/bash
mkdir results

num_proc=20

sbatch --ntasks=$num_proc --mem=50000 --partition=slim  --time=01:10:00 --output=results/mono simulation.job;


