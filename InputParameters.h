#ifndef Libmesh_Parameters_hpp
#define Libmesh_Parameters_hpp

#include "optimization.h"
using namespace libMesh;

class InputParameters
{

public:
   	InputParameters(){}
    ~InputParameters(){}
	bool readParameters(GetPot & infile)
	{

		approx_order_ = infile("approx_order", "FIRST");
		fe_family_ = infile("fe_family", "LAGRANGE");
		parallel_  = infile("parallel",false);

	  // -----------------------------------------------------------------------
	  // mesh's parameters
	  // -----------------------------------------------------------------------
		n_elem_          = infile("n_elem", 10);
		input_mesh_      = infile("input_mesh", false);
		scar_            = infile("scar", false); 
		domain_len_      = infile("domain_len", 1);
		dim_             = infile("dim", 2);
		x_min_           = infile("x_min", 0);
		y_min_           = infile("y_min", 0);
		z_min_           = infile("z_min", 0);
		verbose_BC_      = infile("verbose_BC", false);



	  // -----------------------------------------------------------------------
	  // debugging's parameters
	  // -----------------------------------------------------------------------
		verbose_   		= infile("verbose", false);
		verbose_PerfLog_= infile("verbose_PerfLog", false);
		verbose_plot_   = infile("verbose_plot", false);
		verbose_preconditioner_ = infile("verbose_preconditioner", false);
		verbose_transpose_ = infile("verbose_transpose", false);

	  // -----------------------------------------------------------------------
	  // Monodomain's parameters
	  // -----------------------------------------------------------------------
		eta0_      		= infile("eta0", 150);
		eta1_      		= infile("eta1", 440);
		eta2_      		= infile("eta2", 1.2e-2);
		eta3_      		= infile("eta3", 1);
		vth_       		= infile("vth", 13);
		vpk_       		= infile("vpk", 80);
		C_m_       		= infile("C_m", 1e-2);
		Chi_       		= infile("Chi", 10000);
		T_         		= infile("T",  10);
		dt_      		= infile("dt", 0.000001);
		time_           = infile("time", 0);

	 //  // -----------------------------------------------------------------------
	 //  // Eikonal's parameters
	 //  // -----------------------------------------------------------------------
		c0_         	  = infile("c0", 50);
		tau_        	  = infile("tau", 5); 
		itr_Newton_ 	  = infile("itr_Newton", 100);       
		tolerance_Newton_ = infile("tolerance_Newton", 1e-13);

	  // -----------------------------------------------------------------------
	  // ansazt function to convert the activation time to voltage over the time
	  // -----------------------------------------------------------------------
		gamma_           = infile("gamma", 0);
		D_        		 = infile("D", 0.0001); 
		G_ 		         = infile("G", 1.5);       
		alpha_ansazt_    = infile("alpha_ansazt", 1);

	  // -----------------------------------------------------------------------
	  // pde constrained optimization's parameters
	  // -----------------------------------------------------------------------
		rho_min_   		= infile("rho_min", 1.2);
		rho_max_   		= infile("rho_max", 5);
		beta_      		= infile("beta", 1);

	  // -----------------------------------------------------------------------
	  // gradient descent's parameters
	  // -----------------------------------------------------------------------
		tol_       		= infile("tol", 1e-12);   // termination tolerance
		dxmin_     		= infile("dxmin", 1e-9);   // minimum allowed perturbation
		alpha_     		= infile("alpha_descent", 0.9);    // step size ( 0.33 causes instability, 0.2 quite accurate)
		maxiter_   		= infile("maxiter", 5000); // maximum number of allowed iterations

	  return true;
	}

	
	bool setParameters(EquationSystems & equation_systems)
	{

		equation_systems.parameters.set<std::string>("approx_order")  = approx_order_;
		equation_systems.parameters.set<std::string>("fe_family")     = fe_family_;
		equation_systems.parameters.set<bool>("parallel")             = parallel_;		
		// -----------------------------------------------------------------------
		// mesh's parameters
		// -----------------------------------------------------------------------
		equation_systems.parameters.set<unsigned int>("n_elem") = n_elem_;
		equation_systems.parameters.set<bool>("input_mesh")     = input_mesh_;
		equation_systems.parameters.set<bool>("scar")           = scar_;
		equation_systems.parameters.set<Real>("domain_len")     = domain_len_;
		equation_systems.parameters.set<unsigned int>("dim")    = dim_;
		equation_systems.parameters.set<Real>("x_min")          = x_min_;
		equation_systems.parameters.set<Real>("y_min")          = y_min_;
		equation_systems.parameters.set<Real>("z_min")          = z_min_;


		// -----------------------------------------------------------------------
		// debugging's parameters
		// -----------------------------------------------------------------------
		equation_systems.parameters.set<bool>("verbose")        = verbose_;
		equation_systems.parameters.set<bool>("verbose_PerfLog")= verbose_PerfLog_;
		equation_systems.parameters.set<bool>("verbose_plot")   = verbose_plot_;
		equation_systems.parameters.set<bool>("verbose_preconditioner") = verbose_preconditioner_;
		equation_systems.parameters.set<bool>("verbose_transpose") = verbose_transpose_;

		// -----------------------------------------------------------------------
		// Monodomain's parameters
		// -----------------------------------------------------------------------
		equation_systems.parameters.set<Real>("eta0")        = eta0_;
		equation_systems.parameters.set<Real>("eta1")        = eta1_;
		equation_systems.parameters.set<Real>("eta2")        = eta2_;
		equation_systems.parameters.set<Real>("eta3")        = eta3_;
		equation_systems.parameters.set<Real>("vth")         = vth_;
		equation_systems.parameters.set<Real>("vpk")         = vpk_;
		equation_systems.parameters.set<Real>("C_m")         = C_m_;
		equation_systems.parameters.set<Real>("Chi")         = Chi_;
		equation_systems.parameters.set<unsigned int >("T")   = T_;       
		equation_systems.parameters.set<Real>("dt")        = dt_;
		equation_systems.parameters.set<Real>("time")        = time_;



		// -----------------------------------------------------------------------
		// Eikonal's parameters
		// -----------------------------------------------------------------------
		equation_systems.parameters.set<Real>("c0")                  = c0_;
		equation_systems.parameters.set<Real>("tau")                 = tau_; 
		equation_systems.parameters.set<unsigned int >("itr_Newton") = itr_Newton_;       
		equation_systems.parameters.set<Real>("tolerance_Newton")    = tolerance_Newton_;

		// -----------------------------------------------------------------------
		// ansazt function to convert the activation time to voltage over the time
		// -----------------------------------------------------------------------
		equation_systems.parameters.set<Real>("gamma")        = gamma_;
		equation_systems.parameters.set<Real>("D")   		  = D_; 
		equation_systems.parameters.set<Real>("G")        	  = G_;       
		equation_systems.parameters.set<Real>("alpha_ansazt") = alpha_ansazt_;

		// -----------------------------------------------------------------------
		// pde constrained optimization's parameters
		// -----------------------------------------------------------------------
		equation_systems.parameters.set<Real>("rho_min")      = rho_min_;  // lower bound
		equation_systems.parameters.set<Real>("rho_max")      = rho_max_;  // upper bound
		equation_systems.parameters.set<Real>("beta")         = beta_;     // regularization parameter
		equation_systems.parameters.set<bool>("regu_L2")      = regu_L2_;     // regularization parameter
		equation_systems.parameters.set<bool>("regu_H1")      = regu_H1_;     // regularization parameter
		equation_systems.parameters.set<bool>("regu_Semi")    = regu_Semi_;     // regularization parameter
		

		// -----------------------------------------------------------------------
		// gradient descent's parameters
		// -----------------------------------------------------------------------
		equation_systems.parameters.set<Real>("tol")             = tol_;     // termination tolerance
		equation_systems.parameters.set<unsigned int>("maxiter") = maxiter_; // maximum number of allowed iterations
		equation_systems.parameters.set<Real>("dxmin")           = dxmin_;   // minimum allowed perturbation
		equation_systems.parameters.set<Real>("alpha_descent")   = alpha_;   // step size ( 0.33 causes instability, 0.2 quite accurate)
		return true;
	}
	// -----------------------------------------------------------------------
	// getters
	// -----------------------------------------------------------------------

	// -----------------------------------------------------------------------
	// mesh's parameters
	// -----------------------------------------------------------------------
	std::string   approx_order()   {  return approx_order_; } 
	std::string   fe_family()      {  return fe_family_; } 
	bool parallel()   {  return parallel_; } 
	unsigned int    n_elem()   	   {  return n_elem_; } 
	bool input_mesh()   {  return input_mesh_; } 
	bool scar()   {  return scar_; } 
	Real domain_len()   {  return domain_len_; } 
	unsigned int dim()   {  return dim_; } 
	Real x_min()   {  return x_min_; } 
	Real y_min()   {  return y_min_; } 
	Real z_min()   {  return z_min_; } 
	bool verbose_BC() {return verbose_BC_;}

	// -----------------------------------------------------------------------
	// debugging's parameters
	// -----------------------------------------------------------------------
	bool verbose() {return verbose_;}
	bool verbose_PerfLog() {return verbose_PerfLog_;}
	bool verbose_plot() {return verbose_plot_;}
	bool verbose_preconditioner() {return verbose_preconditioner_;}
	bool verbose_transpose() {return verbose_transpose_;}
	
	// -----------------------------------------------------------------------
	// Monodomain's parameters
	// -----------------------------------------------------------------------
	Real eta0()	{ return eta0_;}
	Real eta1()	{ return eta1_;}
	Real eta2()	{ return eta2_;}
	Real eta3()	{ return eta3_;}
	Real vth()	{ return vth_;}
	Real vpk()	{ return vpk_;}
	Real C_m()	{ return C_m_;}
	Real Chi()	{ return Chi_;}
	unsigned int  T()	{ return T_;}
	Real dt()	{ return dt_;}
	Real time() { return time_;}

	// -----------------------------------------------------------------------
	// Eikonal's parameters
	// -----------------------------------------------------------------------
	Real c0() { return c0_;}
	Real tau() { return tau_;} 
	unsigned int  itr_Newton() { return itr_Newton_;}       
	Real tolerance_Newton() { return tolerance_Newton_;}

	// -----------------------------------------------------------------------
	// ansazt function to convert the activation time to voltage over the time
	// -----------------------------------------------------------------------
	Real gamma() { return gamma_;}
	Real D() { return D_;} 
	Real G() { return G_;}       
	Real alpha_ansazt() { return alpha_ansazt_;}

	// -----------------------------------------------------------------------
	// pde constrained optimization's parameters
	// -----------------------------------------------------------------------
	Real rho_min(){return rho_min_;};
	Real rho_max(){return rho_max_;};
	Real beta(){return beta_;};
	bool regu_L2(){return regu_L2_;};
	bool regu_H1(){return regu_H1_;};
	bool regu_Semi(){return regu_Semi_;};

	// -----------------------------------------------------------------------
	// gradient descent's parameters
	// -----------------------------------------------------------------------
	Real tol(){return tol_;}  // termination tolerance
	Real dxmin(){return dxmin_;}// minimum allowed perturbation
	Real alpha(){return alpha_;} // step size ( 0.33 causes instability, 0.2 quite accurate)
	unsigned int maxiter(){return maxiter_;} // maximum number of allowed iterations

	// -----------------------------------------------------------------------
	// setters
	// -----------------------------------------------------------------------
	void n_elem(unsigned int n_elem)  {   n_elem_ =  n_elem; } 
	void dim(int dim)  {   dim_ =  dim; } 
	void x_min(Real x_min)  {   x_min_ =  x_min; } 
	void y_min(Real y_min)  {   y_min_ =  y_min; } 
	void z_min(Real z_min)  {   z_min_ =  z_min; } 
	void input_mesh(bool input_mesh)  {   input_mesh_ =  input_mesh; } 


private:

	std::string approx_order_;
	std::string fe_family_;
	bool parallel_;

	// -----------------------------------------------------------------------
	// mesh's parameters
	// -----------------------------------------------------------------------
	unsigned int n_elem_;
	bool input_mesh_;
	bool scar_; 
	Real domain_len_;
	unsigned int dim_;
	Real x_min_;
	Real y_min_;
	Real z_min_;
	bool verbose_BC_;

	// -----------------------------------------------------------------------
	// debugging's parameters
	// -----------------------------------------------------------------------
	bool verbose_;
	bool verbose_PerfLog_;
	bool verbose_plot_;
	// -----------------------------------------------------------------------
	// Solve systems with preconditioner
	// -----------------------------------------------------------------------
	bool verbose_preconditioner_;

	// -----------------------------------------------------------------------
	// matrix transpose in utopia
	// -----------------------------------------------------------------------
	bool verbose_transpose_;

	// -----------------------------------------------------------------------
	// Monodomain's parameters
	// -----------------------------------------------------------------------
	Real eta0_;
	Real eta1_;
	Real eta2_;
	Real eta3_;
	Real vth_;
	Real vpk_;
	Real C_m_;
	Real Chi_;
	unsigned int  T_;
	Real dt_;
	Real time_;

	// -----------------------------------------------------------------------
	// Eikonal's parameters
	// -----------------------------------------------------------------------
	Real c0_;
	Real tau_; 
	unsigned int itr_Newton_;       
	Real tolerance_Newton_;

	// -----------------------------------------------------------------------
	// ansazt function to convert the activation time to voltage over the time
	// -----------------------------------------------------------------------
	Real gamma_;
	Real D_; 
	Real G_;       
	Real alpha_ansazt_;

	// -----------------------------------------------------------------------
	// pde constrained optimization's parameters
	// -----------------------------------------------------------------------
	Real rho_min_;
	Real rho_max_;
	Real beta_;
	bool regu_L2_;
	bool regu_H1_;
	bool regu_Semi_;

	// -----------------------------------------------------------------------
	// gradient descent's parameters
	// -----------------------------------------------------------------------
	Real tol_;  // termination tolerance
	Real dxmin_; // minimum allowed perturbation
	Real alpha_; // step size ( 0.33 causes instability, 0.2 quite accurate)
	unsigned int maxiter_; // maximum number of allowed iterations


};

#endif